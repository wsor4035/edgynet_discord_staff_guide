# Edgynet Official Discord staff guide
here is a guide with some basic outlines. Moderators and/or Admins should try to quote back to https://edgy1.net/tos when possible
note this guide is subject to change and improvements

## Roles:

## Admin
Admins are responsible for server maintenance.

## Moderator
Moderators are responsible for making sure all rules in https://edgy1.net/tos are followed, and to take action if the server suffers any spam attacks

## Streamers
A special role given by request to streamers who intend to stream minetest, multicraft, or other games.

## Server owners
give this role to people who own a mt/mc server

## Builders Club
gives this role to memebers of Agura's builders club

## Devolpers
give to people who work on modding or development for minetest and multicraft.


## Streaming 
keep talk related to strreaming here(shoot !rank people)

insert other categories here at a later date

## Discord Bots

Dyno automatically deletes and warns all caps msg's from discord as well as repeat msg's, please use dyno for moderation

## Dyno aka dinosaur mitochondria
command list: https://dyno.gg/commands
prefix: ?
preferred for bans. use `?ban @user reason`

## mee6 aka YOU7.0-dev
Preferred for warning users. use: `!warn @user reason`

## Lock bot
command list: https://support.lockbot.network/lock-commands/link-lock
use its commands to lock and unlock a discord channel in case of huge spam or a raid happening, then unlock when over or problem is delt with

## Relay Bots
* BlockCity Relay
* catlandia-relay
* Dragons Relay
* EdgyIRC chat
* EdgyIRC relay
* FrostyRelay
* IRC relay
* Unknown Relay
* Vineland Relay

These bots are here to relay chat from irc to discord(chat from game being sent to irc). typically don't need to worry about them, if down, notify edgy

## DISBOARD
allows you to !bump to bump your discord server to the top of there discord server list

## EdgyBot 
General bot based on lurklite with a few commands. may be expanded later. shouldn't need to be used by staff

## lurk(discord)
...uh, its lurk :P
suports limited subset of commands at: https://luk3yx.github.io/lurk   
^luk3yx, seperate url for discord lurk commands perhaps?

## Member Count
nice bot that displays memeber count, bot count, and user count at the top of channels list(at least as of writing), need not worry about this bot, should be able to ignore it and let it do its job

## Rythm
bot that provides music to voice channels. do note, user must be in a voice channel to use it, if a user complains they can't use it, check first to see if there in a voice channel.

## General procedures

For minor violations such as spaming or inappropriate usage of chat, use `!warn @user reason`. 3 warns results in a tempmute. Mee6 will automatically warn users for certain spamming paterns.
For a major TOS violation that does not require an investigation, or serious trolling by a new user, use `?ban @user` reason
For a major TOS violation that requires an investigation, or serious trolling by a longtime user, message Edgy1 or luk3yx.


## Credits
* author: wsor 
* co-author: edgy
* editors: none

written: 7/15/2020
last-updated: 7/19/2020

## Todo:
* see notes listed in each section
* add genereal irc section about how to use it, different edgy platforms when it comes to irc(_irc, _lounge, _znc)
* add more/update info as time goes on